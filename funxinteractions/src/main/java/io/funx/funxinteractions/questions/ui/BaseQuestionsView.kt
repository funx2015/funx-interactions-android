package io.funx.funxinteractions.questions.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.TextView
import cn.refactor.lib.colordialog.PromptDialog
import io.funx.funxinteractions.R
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.content.presenter.ParticipateListener
import io.funx.funxinteractions.questions.model.OptionModel
import io.funx.funxinteractions.questions.model.Question
import io.funx.funxinteractions.questions.model.QuestionsModel
import io.funx.funxinteractions.questions.model.VoteModel
import io.funx.funxinteractions.questions.presenter.BaseQuestionsListener
import io.funx.futbol.core.webservice.builder
import io.funx.futbol.core.webservice.onError
import io.funx.futbol.core.webservice.onResponse
import io.funx.futbol.core.webservice.request


abstract class BaseQuestionsView(context: Context) : FrameLayout(context),
        BaseQuestionsListener {

    lateinit var project: Project
    private lateinit var userId: String
    private var controller = QuestionsModel()
    private var progress: View? = null
    private var typefaceNormal: Typeface? = Typeface.DEFAULT
    private var typefaceBold: Typeface? = Typeface.DEFAULT_BOLD
    private var listenerParticipate: ParticipateListener? = null


    override fun sendResponse(position: Int, op: OptionModel, view: View?, hasNext: Boolean) {
        if (project.questions[position].vote != null) return

        val vote = VoteModel(isCorrect = op.isCorrect, option = op.id.toString(), user = userId)

        this.project.questions[position].vote = vote
        this.showLoader()

        controller.sendVoteAsync(this.project.questions[position].id, vote).builder()
                .onResponse {
                    hideLoader()
                    project.questions[position].vote = it
                    voteResult(position, it, hasNext, view = view)
                }.onError {
                    it.printStackTrace()
                    showError()
                }.request()
    }

    override fun instanceProject(project: Project, userId: String, normalFont: Typeface?, boldFont: Typeface?) {
        this.project = project
        this.userId = userId
        this.typefaceNormal = normalFont
        this.typefaceBold = boldFont

    }


    override fun showPopUp(question: Question, voteModel: VoteModel, hasNext: Boolean) {
        val dialog = PromptDialog(context)
                .setAnimationEnable(true)

        var type: Int = PromptDialog.DIALOG_TYPE_INFO
        var show = true
        var title = ""
        var text = ""

        when (question.getTypeAlert()) {
            Question.AlertType.INFO -> {
                type = PromptDialog.DIALOG_TYPE_DEFAULT
                text = question.infoMessage
            }

            Question.AlertType.NONE -> {
                show = false
            }

            Question.AlertType.QUESTION -> {
                type = if (voteModel.isCorrect) {
                    text = question.correctMessage
                    PromptDialog.DIALOG_TYPE_SUCCESS
                } else {
                    title = question.incorrectMessage
                    text = "La respuesta correcta es: " + question.getCorrectionOption()
                    PromptDialog.DIALOG_TYPE_WRONG
                }
            }

        }
        if (show) {
            dialog.setTitleText(title)
                    .setDialogType(type)
                    .setContentText(text)
                    .setPositiveListener("Listo") {
                        it.dismiss()
                        onDismissDialog(true)
                    }
                    .setOnDismissListener {
                        listenerParticipate?.onDismissPopUp(hasNext)
                    }

            dialog.setCanceledOnTouchOutside(false)
            dialog.show()
            dialog.contentTextView?.textAlignment = View.TEXT_ALIGNMENT_CENTER
            dialog.contentTextView?.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18F)
            dialog.contentTextView?.isSingleLine = false
            dialog.contentTextView?.typeface = typefaceNormal
            dialog.titleTextView?.typeface = typefaceBold
            dialog.titleTextView?.ellipsize = null
            dialog.titleTextView?.isSingleLine = false


        } else {
            listenerParticipate?.onDismissPopUp(hasNext)
        }


    }

    override fun voteResult(position: Int, voteModel: VoteModel, hasNext: Boolean, view: View?) {
        listenerParticipate?.onVote(voteModel)
        showPopUp(project.questions[position], voteModel, hasNext)
    }


    override fun showError() {
        hideLoader()
    }

    fun onResponseParticipate(participateListener: ParticipateListener) {
        this.listenerParticipate = participateListener
    }

    @SuppressLint("InflateParams")
    override fun showLoader() {
        val root = this.rootView?.findViewById<ViewGroup>(android.R.id.content)
        if (root != null) {
            if (progress == null) {
                progress = LayoutInflater.from(context)
                        .inflate(R.layout.custom_progress, null)
            }
            root.addView(progress)
        }
    }

    override fun hideLoader() {
        val root = this.rootView?.findViewById<ViewGroup>(android.R.id.content)

        if (progress != null && root != null) {
            root.removeView(progress)
        }
    }

    fun setFont(vararg textViews: TextView?) {

        if (typefaceBold == null) return
        if (typefaceNormal == null) return
        for (element in textViews) {
            if (element?.typeface != null) {
                element.typeface = if (element.typeface.isBold) {
                    typefaceBold
                } else {
                    typefaceNormal
                }
            }
        }

    }

    open fun onDismissDialog(dismiss: Boolean) = Unit

}