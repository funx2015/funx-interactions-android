package io.funx.funxinteractions.questions.model.deserializer

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import io.funx.funxinteractions.questions.model.VoteModel
import java.lang.reflect.Type


class VoteDeserializer : JsonDeserializer<Any>{


    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Any {
        val vote = json?.asJsonObject?.get("vote")

        return if (json?.asJsonObject?.get("vote") != null) {
            Gson().fromJson(json.asJsonObject.get("vote"), VoteModel::class.java)
        } else {
            Gson().fromJson(vote, VoteModel::class.java)
        }
    }
}