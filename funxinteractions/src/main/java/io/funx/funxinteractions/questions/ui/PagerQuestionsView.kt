package io.funx.funxinteractions.questions.ui

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.RelativeLayout
import android.widget.ScrollView
import android.widget.TextView
import androidx.core.widget.NestedScrollView
import com.viewpagerindicator.CirclePageIndicator
import io.funx.funxinteractions.R
import io.funx.funxinteractions.base.custom.WrapContentHeightViewPager
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.questions.model.Question
import io.funx.funxinteractions.questions.model.VoteModel

class PagerQuestionsView(context: Context) : BaseQuestionsView(context) {

    private lateinit var content: RelativeLayout
    private lateinit var adapter: QuestionPagerAdapter
    private var vp: WrapContentHeightViewPager? = null

    @SuppressLint("InflateParams")
    override fun initView() {
        val scroll = NestedScrollView(context)
        scroll.isFillViewport = true
        content = LayoutInflater.from(context).inflate(R.layout.content_one, null) as RelativeLayout
        content.findViewById<RelativeLayout>(R.id.main).setBackgroundColor(project.colors.backgroundAll.color)


        val topText = content.findViewById<TextView>(R.id.top_text)
        topText.setTextColor(project.colors.topTextColor.color)
        topText.setBackgroundColor(project.colors.topTextBackground.color)
        topText.text = project.topText
        if (project.topText == "") {
            topText.visibility = View.GONE
        }
        adapter = instanceAdapter(project)
        vp = content.findViewById(R.id.vp)
        vp?.adapter = adapter

        setFont(topText)
        val vpIndicator = content.findViewById<CirclePageIndicator>(R.id.page_indicator)
        vpIndicator.setViewPager(vp)

        vpIndicator.visibility = if (project.questions.size > 1) {
            View.VISIBLE
        } else {
            View.GONE
        }

        scroll.addView(content)
        addView(scroll)

    }


    private fun instanceAdapter(project: Project) : QuestionPagerAdapter {
        val adapter = if (project.blockSwipe) {
            val list = arrayListOf<Question>()
            var last = true
            project.questions.forEach {
                if (it.vote != null) {
                    list.add(it)
                } else {
                    if (last) {
                        last = false
                        list.add(it)
                    }
                }
            }
            if (list.isEmpty()) list.add(project.questions.first())
            QuestionPagerAdapter(this, list = list)
        } else {
            QuestionPagerAdapter(this)
        }
        this.adapter = adapter
        return adapter
    }


    override fun onDismissDialog(dismiss: Boolean) {
        val current = vp?.currentItem ?: 0
        if (project.questions.size > current && dismiss) {
            vp?.currentItem = current + 1
        }
    }

    override fun voteResult(position: Int, voteModel: VoteModel, hasNext: Boolean, view: View?) {
        var next = hasNext
        if (adapter.list.size < project.questions.size) {
            vp?.adapter = instanceAdapter(project)
            vp?.currentItem = adapter.list.lastIndex
            next = true
        }
        super.voteResult(position, voteModel, next, view)
        adapter.voteResult(position, voteModel, view)

    }

}