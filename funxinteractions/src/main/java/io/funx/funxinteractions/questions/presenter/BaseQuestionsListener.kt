package io.funx.funxinteractions.questions.presenter

import android.graphics.Typeface
import android.view.View
import io.funx.funxinteractions.questions.model.OptionModel
import io.funx.funxinteractions.questions.model.Question
import io.funx.funxinteractions.questions.model.VoteModel
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.contest.model.ResponseParticipate

interface BaseQuestionsListener {


    fun sendResponse(position: Int, op: OptionModel, view: View?, hasNext: Boolean)

    fun instanceProject(project: Project, userId: String, normalFont: Typeface?, boldFont: Typeface?)

    fun initView()

    fun showPopUp(question: Question, voteModel: VoteModel, hasNext: Boolean)

    fun showError()

    fun voteResult(position: Int, voteModel: VoteModel, hasNext: Boolean, view: View? = null)

    fun showLoader()

    fun hideLoader()



}