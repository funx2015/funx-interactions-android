package io.funx.funxinteractions.questions.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList


@Parcelize
data class Question(

        var id: Int = 0,
        var text: String = "",
        var points: Int = 0,
        var difficulty: Int = 0,
        var state: String = "",
        var image: String? = "",
        var vote: VoteModel? = null,

        @SerializedName("alert_type")
        var typeAlert: String = "",

        @SerializedName("info_message")
        var infoMessage: String = "",

        @SerializedName("game_type")
        var game_type: String = "",

        @SerializedName("start_at")
        var startAt: Date = Date(),

        @SerializedName("initial_message")
        var initialMessage: String = "",

        @SerializedName("incorrect_message")
        var incorrectMessage: String = "",

        @SerializedName("stop_at")
        var stopAt: Date = Date(),

        @SerializedName("correct_message")
        var correctMessage: String = "",

        var options: ArrayList<OptionModel> = arrayListOf(),

        @SerializedName("state_display")
        var stateDisplay: String = ""


) : Parcelable {

        fun getCorrectionOption() : String {
                for (i in 0 until options.size) {
                        if (options[i].isCorrect) {
                                return options[i].text
                        }
                }
                return ""
        }

        enum class AlertType {
                NONE, QUESTION, INFO
        }


        fun getTypeAlert() = try {
                AlertType.valueOf(typeAlert.toUpperCase())
        } catch (e: Exception) {
                AlertType.INFO
        }
}