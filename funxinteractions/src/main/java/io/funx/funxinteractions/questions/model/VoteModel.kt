package io.funx.funxinteractions.questions.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.funx.funxinteractions.contest.model.Finish
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VoteModel(
        var id: Int = 0,
        var user: String = "",
        var option: String = "",
        var points: Int = 0,

        @SerializedName("final")
        var finish: Finish? = null,

        @SerializedName("is_correct")
        var isCorrect: Boolean = false

) : Parcelable