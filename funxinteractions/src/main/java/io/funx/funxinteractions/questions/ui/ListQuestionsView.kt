package io.funx.funxinteractions.questions.ui

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import io.funx.funxinteractions.R
import io.funx.funxinteractions.base.extensions.ButtonStatus
import io.funx.funxinteractions.base.extensions.convertDpToPixel
import io.funx.funxinteractions.base.extensions.statusButtonQuestion
import io.funx.funxinteractions.contest.model.ResponseParticipate
import io.funx.funxinteractions.questions.model.VoteModel

class ListQuestionsView(context: Context) : BaseQuestionsView(context) {

    private var layoutInflater: LayoutInflater = LayoutInflater.from(context)

    @SuppressLint("InflateParams")
    override fun initView() {
        val root = LinearLayout(context)
        root.orientation = LinearLayout.VERTICAL

        val slp = LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)


        for (i in project.questions.indices) {
            val content = layoutInflater.inflate(R.layout.content_two, null) as RelativeLayout
            content.setBackgroundColor(project.colors.backgroundContent.color)

            val optionsLinearLayout = content.findViewById<LinearLayout>(R.id.options)

            val optionsList = project.questions[i].options

            val questionTextView = content.findViewById<TextView>(R.id.question)
            questionTextView.text = project.questions[i].text
            questionTextView.setTextColor(project.colors.questionTextColor.color)

            val vote = project.questions[i].vote

            if (optionsList.size > 3) optionsLinearLayout.orientation = LinearLayout.VERTICAL

            setFont(questionTextView)

            var layout: LinearLayout? = null
            if (optionsList.size > 3) {
                layout = createLinearLayout()
            }

            for (j in optionsList.indices) {
                val optionButton = layoutInflater.inflate(R.layout.item_option_two, null) as Button
                val option = optionsList[j]
                setFont(optionButton)

                var status = ButtonStatus.NORMAL

                optionButton.setOnClickListener {
                    sendResponse(i, option, it, project.questions.lastIndex == i)
                }
                if (vote != null) {
                    if (project.questions[i].options[j].id == vote.option.toInt()) {
                        status = if (vote.isCorrect) {
                            ButtonStatus.CORRECT
                        } else {
                            ButtonStatus.ERROR
                        }
                    }

                }
                statusButtonQuestion(optionButton, status, project)

                optionButton.setTextColor(project.colors.optionTextColor.color)
                optionButton.text = option.text

                val m = convertDpToPixel(5f)
                val llp = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f)
                llp.setMargins(m, m, m, m)

                if (optionsList.size > 3 && layout != null) {
                    layout.addView(optionButton, llp)
                    if ((j + 1) % 2 == 0) {
                        optionsLinearLayout.addView(layout)
                        layout = createLinearLayout()
                    }
                } else {
                    optionsLinearLayout.addView(optionButton, llp)
                }
            }

            root.addView(content)
        }

        addView(root, slp)

    }

    override fun voteResult(position: Int, voteModel: VoteModel,hasNext: Boolean, view: View?) {
        super.voteResult(position, voteModel,hasNext, view)
        if (view != null) {
            statusButtonQuestion(view as Button,
                    if (voteModel.isCorrect) ButtonStatus.CORRECT
                    else ButtonStatus.ERROR,
                    project
            )

        }
    }


    private fun createLinearLayout(): LinearLayout {
        val layout = LinearLayout(context)
        layout.orientation = LinearLayout.HORIZONTAL
        val lp = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
        layout.layoutParams = lp

        return layout
    }

}