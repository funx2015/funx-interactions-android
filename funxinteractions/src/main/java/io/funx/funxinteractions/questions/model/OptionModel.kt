package io.funx.funxinteractions.questions.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OptionModel (

        var id: Int = 0,
        var text: String = "",

        @SerializedName("is_correct")
        var isCorrect: Boolean = false,
        var percent: Float
) : Parcelable