package io.funx.funxinteractions.questions.model

import io.funx.funxinteractions.base.webservice.createServiceInteractions
import kotlinx.coroutines.Deferred

class QuestionsModel {


    private val service = createServiceInteractions()

    fun sendVoteAsync(questionId: Int, voteModel: VoteModel): Deferred<VoteModel> {
        return service.voteAsync(questionId, voteModel)
    }


}
