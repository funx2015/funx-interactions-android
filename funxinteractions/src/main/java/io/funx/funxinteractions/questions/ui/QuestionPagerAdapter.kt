package io.funx.funxinteractions.questions.ui

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.nostra13.universalimageloader.core.ImageLoader
import io.funx.funxinteractions.R
import io.funx.funxinteractions.base.extensions.ButtonStatus
import io.funx.funxinteractions.base.extensions.getBasicImageLoaderListener
import io.funx.funxinteractions.base.extensions.statusButtonQuestion
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.questions.model.Question
import io.funx.funxinteractions.questions.model.VoteModel

class QuestionPagerAdapter(
        private val interactionView: BaseQuestionsView,
        val project: Project = interactionView.project,
        var list: ArrayList<Question> = interactionView.project.questions
) : androidx.viewpager.widget.PagerAdapter() {

    @SuppressLint("InflateParams")
    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val item = LayoutInflater.from(container.context).inflate(R.layout.item_pager, container, false)
        val project = interactionView.project
        val elem = list[position]
        val text = list[position].text
        val options1 = list[position].options
        val image1 = list[position].image
        val vote = list[position].vote
        val question = item.findViewById<TextView>(R.id.question)
        val options = item.findViewById<LinearLayout>(R.id.options)
        val image = item.findViewById<ImageView>(R.id.image)

        item.setBackgroundColor(project.colors.backgroundContent.color)
        question.setTextColor(project.colors.questionTextColor.color)
        question.text = text

        interactionView.setFont(question)
        ImageLoader.getInstance().displayImage(image1, image, getBasicImageLoaderListener())

        for (i in 0 until options1.size) {
            val optionView = LayoutInflater.from(container.context).inflate(R.layout.item_option_one, null)
            var statusButton = ButtonStatus.NORMAL
            val option = optionView.findViewById<Button>(R.id.optionBtn)
            option.setTextColor(project.colors.optionTextColor.color)
            option.text = options1[i].text
            option.setOnClickListener {
                interactionView.sendResponse(position, elem.options[i], option, list.last() == elem)
            }
            interactionView.setFont(option)

            if (vote != null) {
                if (elem.options[i].id == vote.option.toInt()) {
                    statusButton = if (vote.isCorrect) ButtonStatus.CORRECT else ButtonStatus.ERROR
                }
            }
            statusButtonQuestion(option, statusButton, project)
            options.addView(optionView)
        }
        container.addView(item)
        return item
    }

    override fun getCount(): Int {
        return list.size
    }

    override fun isViewFromObject(view: View, obj: Any): Boolean {
        return view === obj
    }

    override fun destroyItem(container: ViewGroup, position: Int, any: Any) {
        container.removeView(any as View)
    }

    override fun getItemPosition(`object`: Any): Int {
        return POSITION_NONE
    }

    fun voteResult(position: Int, voteModel: VoteModel, view: View?) {
        if (view != null) {
            statusButtonQuestion(view as Button?,
                    if (voteModel.isCorrect) ButtonStatus.CORRECT
                    else ButtonStatus.ERROR,
                    project
            )

        }

    }


}
