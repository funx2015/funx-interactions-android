package io.funx.funxinteractions.base.model

data class Ranking(
        var answers: Int = 0,
        var points: Int = 0,
        var rank: Int = 0,
        var user: String = "",
        var text: String = ""
)