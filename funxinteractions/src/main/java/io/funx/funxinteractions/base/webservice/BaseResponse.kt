package io.funx.funxinteractions.base.webservice

import com.google.gson.GsonBuilder
import io.funx.funxinteractions.content.model.FunxColor
import io.funx.funxinteractions.content.model.FunxColorDeserializer
import io.funx.futbol.core.webservice.ApiClient


fun createServiceInteractions(): InteractionService {
    return ApiClient.createService(
            getBaseUrlInteractions(),
            InteractionService::class.java,
            GsonBuilder().registerTypeAdapter(FunxColor::class.java, FunxColorDeserializer())
    )
}


fun getBaseUrlInteractions() : String {
    return "https://interactions.funx.io/"
}
