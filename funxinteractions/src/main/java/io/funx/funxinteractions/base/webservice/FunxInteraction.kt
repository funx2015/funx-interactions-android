package io.funx.funxinteractions.base.webservice

import io.funx.funxinteractions.base.model.Ranking
import io.funx.funxinteractions.content.model.Project
import io.funx.futbol.core.webservice.builder
import io.funx.futbol.core.webservice.onError
import io.funx.futbol.core.webservice.onResponse
import io.funx.futbol.core.webservice.request

@Deprecated("use new callback")
class FunxInteraction {

    private val service = createServiceInteractions()


    fun getProject(projectId: String, userId: String, result: (Project) -> Unit) {
        service.getProjectAsync(projectId, userId)
                .builder()
                .onResponse {
                    result.invoke(it)
                }.onError {
                    it.printStackTrace()
                }.request()
    }

    fun getRanking(appId: String, userId: String,
                   result: (Ranking?) -> Unit) {

        service.getRankingAsync(appId, userId)
                .builder()
                .onResponse {
                    result.invoke(it)
                }.onError {
                    result.invoke(null)
                }.request()


    }


    fun getLegendRelation(appId: String, userId: String, result: (Ranking) -> Unit) {

        service.getLegendRelationAsync(appId, userId)
                .builder()
                .onResponse {
                    result.invoke(it)
                }.onError {
                    it.printStackTrace()
                }.request()
    }

    fun getLegend(appId: String, userId: String, result: (Ranking) -> Unit) {

        service.getLegendAsync(appId, userId)
                .builder()
                .onResponse {
                    result.invoke(it)
                }.onError {
                    it.printStackTrace()
                }.request()
    }
}