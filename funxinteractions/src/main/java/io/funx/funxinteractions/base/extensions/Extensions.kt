package io.funx.funxinteractions.base.extensions

import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.RippleDrawable
import android.os.Build
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import android.util.DisplayMetrics
import android.view.View
import android.widget.Button
import com.nostra13.universalimageloader.core.assist.FailReason
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener
import io.funx.funxinteractions.R
import io.funx.funxinteractions.content.model.Project
import androidx.core.graphics.drawable.DrawableCompat
import android.graphics.drawable.Drawable
import com.nostra13.universalimageloader.core.DisplayImageOptions


fun convertStringToColor(color: String?) : Int {

    if (color == null) return Color.TRANSPARENT
    val params = color.split("-")
    return if (params.size == 4) {

        val red = Integer.parseInt(params[0])
        val green = Integer.parseInt(params[1])
        val blue = Integer.parseInt(params[2])
        val alpha = (params[3].toFloat() * 255F).toInt()

        Color.argb(alpha, red, green, blue)

    } else {
        Color.alpha(0)
    }
}


fun getBasicImageLoaderListener() : ImageLoadingListener {

    return object :ImageLoadingListener {
        override fun onLoadingComplete(imageUri: String?, view: View?, loadedImage: Bitmap?) {

        }

        override fun onLoadingStarted(imageUri: String?, view: View?) {

        }

        override fun onLoadingCancelled(imageUri: String?, view: View?) {
            view?.visibility = View.GONE
        }

        override fun onLoadingFailed(imageUri: String?, view: View?, failReason: FailReason?) {
            view?.visibility = View.GONE
        }

    }
}

enum class ButtonStatus {
    NORMAL, CORRECT, ERROR
}


fun statusButtonQuestion(btn: Button?, status: ButtonStatus, project: Project) {

    if (btn == null) return
    val drawable = ContextCompat.getDrawable(btn.context, R.drawable.shape_button) as GradientDrawable

    drawable.setColor(
            when (status) {
                ButtonStatus.NORMAL -> project.colors.optionBackground.color
                ButtonStatus.CORRECT -> project.colors.optionBackgroundCorrect.color
                else -> project.colors.optionBackgroundError.color
            }
    )
    btn.background = drawable
}


fun convertDpToPixel(dp: Float): Int {
    val resources = Resources.getSystem()
    val metrics = resources.displayMetrics
    return Math.round(dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT))
}


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
fun getPressedColorRippleDrawable(normalColor: Int, pressedColor: Int): RippleDrawable {
    return RippleDrawable(getPressedColorSelector(normalColor, pressedColor), getColorDrawableFromColor(normalColor), null)
}

private fun getPressedColorSelector(normalColor: Int, pressedColor: Int): ColorStateList {
    return ColorStateList(
            arrayOf(
                    intArrayOf(android.R.attr.state_pressed),
                    intArrayOf(android.R.attr.state_focused),
                    intArrayOf(android.R.attr.state_activated),
                    intArrayOf()),

            intArrayOf(
                    pressedColor,
                    pressedColor,
                    normalColor,
                    normalColor
            )
    )
}

private fun getColorDrawableFromColor(color: Int): ColorDrawable {
    return ColorDrawable(color)
}



fun setTint(d: Drawable, color: Int): Drawable {
    val wrappedDrawable = DrawableCompat.wrap(d)
    DrawableCompat.setTint(wrappedDrawable, color)
    return wrappedDrawable
}