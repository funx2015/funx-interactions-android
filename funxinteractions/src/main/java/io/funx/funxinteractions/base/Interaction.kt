package io.funx.funxinteractions.base

import android.graphics.Typeface
import io.funx.funxinteractions.base.webservice.createServiceInteractions
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.content.ui.FunxContestView
import io.funx.funxinteractions.content.ui.FunxInteractionContent
import io.funx.funxinteractions.contest.ui.ButtonContest
import io.funx.futbol.core.webservice.builder
import io.funx.futbol.core.webservice.onError
import io.funx.futbol.core.webservice.onResponse
import io.funx.futbol.core.webservice.request

class Interaction(
        var idProject: String,
        var uidUserFirebase: String
) {


    init {
        if (idProject == "") throw IllegalArgumentException("idProject not empty")
        if (uidUserFirebase == "") throw IllegalArgumentException("uidUserFirebase not empty")
    }

    var containQuestion: (Boolean) -> Unit = {}
    var onLoadProgress: () -> Unit = {}
    var onLoadFinish: (Project?) -> Unit = {}
    var fontNormal = Typeface.DEFAULT
    var fontBold = Typeface.DEFAULT_BOLD


    fun init() {
        load {}
    }

    fun init(buttonInteraction: ButtonContest) {
        load {
            buttonInteraction.boldFont = fontBold
            buttonInteraction.normalFont = fontNormal
            buttonInteraction.initButton(idProject, uidUserFirebase)
        }
    }

    fun init(viewInteraction: FunxContestView) {
        load {
            viewInteraction.boldFont = fontBold
            viewInteraction.normalFont = fontNormal
            viewInteraction.start(project = it, user = uidUserFirebase)
        }
    }

    fun init(interactionContent: FunxInteractionContent) {
        load {
            interactionContent.boldFont = fontBold
            interactionContent.normalFont = fontNormal
            interactionContent.start(idProject, user = uidUserFirebase)
        }
    }

    fun init(interactionContent: FunxInteractionContent, project: Project) {
        interactionContent.boldFont = fontBold
        interactionContent.normalFont = fontNormal
        interactionContent.start(project, user = uidUserFirebase)
    }


    fun load(onResponse: (Project) -> Unit) {
        onLoadProgress()
        createServiceInteractions().getProjectAsync(idProject, uidUserFirebase).builder()
                .onResponse {
                    onResponse.invoke(it)
                    containQuestion.invoke(it.questions.isNotEmpty())
                    onLoadFinish(it)
                }.onError {
                    containQuestion.invoke(false)
                    onLoadFinish(null)
                }.request()
    }


}