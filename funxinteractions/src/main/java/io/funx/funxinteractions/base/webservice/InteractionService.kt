package io.funx.funxinteractions.base.webservice

import com.google.gson.JsonObject
import io.funx.funxinteractions.base.model.Ranking
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.contest.model.ResponseParticipate
import io.funx.funxinteractions.questions.model.VoteModel
import kotlinx.coroutines.Deferred
import retrofit2.http.*

interface InteractionService {


    @GET("api/ranking/{appId}/{userId}")
    fun getRankingAsync(
            @Path("appId") appId: String,
            @Path("userId") userId: String
    ): Deferred<Ranking>


    @GET("api/get-legend-relation/{appId}/{userId}")
    fun getLegendRelationAsync(
            @Path("appId") appId: String,
            @Path("userId") userId: String
    ): Deferred<Ranking>


    @GET("api/get-legend/{appId}/{userId}")
    fun getLegendAsync(
            @Path("appId") appId: String,
            @Path("userId") userId: String
    ): Deferred<Ranking>


    @GET("api/v2/get-project/{projectId}/")
    fun getProjectAsync(
            @Path("projectId") projectId: String,
            @Query("user") userId: String
    ): Deferred<Project>


    @GET("/api/v2/get-project/{idProject}/")
    fun getProjectAsync(
            @Path("idProject") idProject: String,
            @Query("user") user: String,
            @Query("relation") relation: String = ""
    ): Deferred<Project>

    @Headers("Content-Type: application/json")
    @POST("api/v2/user-action/{idProject}/")
    fun participateAsync(
            @Path("idProject") idProject: String,
            @Body data: JsonObject
    ): Deferred<ResponseParticipate>

    @POST("/api/v2/send-vote/{questionId}/")
    fun voteAsync(
            @Path("questionId") questionId: Int,
            @Body voteModel: VoteModel
    ): Deferred<VoteModel>
}