package io.funx.funxinteractions.contest.presenter

import android.annotation.SuppressLint
import android.content.Context
import io.funx.funxinteractions.base.webservice.createServiceInteractions
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.contest.model.ContestModel
import io.funx.funxinteractions.contest.model.Finish
import io.funx.futbol.core.mvp.BaseMvpPresenterImpl
import io.funx.futbol.core.webservice.builder
import io.funx.futbol.core.webservice.onError
import io.funx.futbol.core.webservice.onResponse
import io.funx.futbol.core.webservice.request

@SuppressLint("CheckResult")
class ContestPresenter : BaseMvpPresenterImpl<ContestContract.View>(), ContestContract.Presenter {

    private var project: Project? = null
    private var uidUser: String = ""
    private var idProject: String = ""
    lateinit var model: ContestModel

    override fun setProject(project: Project, uidUser: String, idProject: String) {

        model = ContestModel()
        this.project = project
        this.uidUser = uidUser
        this.idProject = idProject

        when (project.getState()) {
            Project.States.ACTIVE, Project.States.INACTIVE -> {
                mView?.showWelcome(project)
            }
            Project.States.PARTIAL -> {
                mView?.showTrivia(project, uidUser)
            }
            Project.States.PARTICIPATING, Project.States.FINISHED -> {
                if (project.finish != null) {
                    mView?.showFinish(project)
                } else {
                    mView?.showTrivia(project, uidUser)
                }
            }
        }

    }

    override fun getProject(context: Context, userId: String, idProject: String) {
        mView?.showProgress()
        this.uidUser = userId
        this.idProject = idProject
        val service = createServiceInteractions()

        service.getProjectAsync(idProject, userId).builder().onResponse {
            mView?.hideProgress()
            setProject(it, uidUser, idProject)
            mView?.onResponseProject(it)
        }.onError {
            it.printStackTrace()
            mView?.hideProgress()
        }.request()
    }

    override fun onParticipateClick() {
        if (project != null) {
            model.onParticipateClick(idProject, uidUser, "click-welcome").builder()
                    .onError {
                        it.printStackTrace()
                        mView?.hideProgress()
                    }
                    .onResponse {
                        mView?.hideProgress()
                        onResponseParticipate(it.final)
                    }.request()
        }
    }

    override fun onDismissPopUp(dismiss: Boolean, hasNext: Boolean) {
        if (!hasNext) {
            if (project?.finish != null) {
                mView?.showFinish(project!!)
            }
        }
    }

    override fun onBackPressed() {
        mView?.finishConcourse(project)
    }

    override fun onResponseParticipate(finish: Finish?) {
        if (project == null) return
        project?.finish = finish
        if (project!!.questions.isNotEmpty()) {
            mView?.showTrivia(project!!, uidUser)
        } else {
            mView?.showFinish(project!!)
        }

    }

}