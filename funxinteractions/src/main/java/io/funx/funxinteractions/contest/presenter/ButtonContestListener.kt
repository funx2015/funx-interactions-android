package io.funx.funxinteractions.contest.presenter

import android.content.Intent
import android.graphics.drawable.Drawable

interface ButtonContestListener {

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?)


}