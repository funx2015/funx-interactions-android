package io.funx.funxinteractions.contest.ui

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import io.funx.funxinteractions.R
import io.funx.funxinteractions.content.model.Project
import kotlinx.android.synthetic.main.dialog_funx_contest.*


open class FunxContestDialog : DialogFragment() {


    companion object {
        @JvmStatic
        val RESULT_CONTEST = 345
    }

    var normalFont: Typeface = Typeface.DEFAULT
    var boldFont: Typeface = Typeface.DEFAULT_BOLD

    override fun onCreate(savedInstanceState: Bundle?) {
        setStyle(STYLE_NORMAL, android.R.style.Theme_Material_Light_DialogWhenLarge_NoActionBar)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.dialog_funx_contest, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        view.setBackgroundColor(Color.TRANSPARENT)
        contestView.boldFont = boldFont
        contestView.normalFont = normalFont
        arguments?.getParcelable<Project?>("project")?.let {
            contestView?.start(it, arguments?.getString("uid") ?: "")
            backButton.setOnClickListener { _ -> finish(it) }
        }
        contestView?.onFinishConcourse = { finish(it) }
    }

    private fun finish(project: Project?) {
        val intent = Intent()
        intent.putExtra("project", project)
        activity?.setResult(Activity.RESULT_OK, intent)
        dismiss()
    }


}