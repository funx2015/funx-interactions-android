package io.funx.funxinteractions.contest.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Welcome (

        var title :String = "",

        @SerializedName("nav")
        var toolbarText: String = "",

        var image: String? = "",

        var text: String = "",

        @SerializedName("button_text")
        var buttonText: String = ""

) : Parcelable