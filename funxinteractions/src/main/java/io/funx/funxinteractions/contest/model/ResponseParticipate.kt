package io.funx.funxinteractions.contest.model

import android.os.Parcelable
import io.funx.funxinteractions.questions.model.VoteModel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseParticipate (
        var project: Int = -1,
        var event: String = "",
        var final: Finish? = null,
        var user: String = ""

) : Parcelable
