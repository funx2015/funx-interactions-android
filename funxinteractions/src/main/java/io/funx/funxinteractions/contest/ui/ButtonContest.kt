package io.funx.funxinteractions.contest.ui

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.ImageButton
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.fragment.app.FragmentActivity
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import io.funx.funxinteractions.R
import io.funx.funxinteractions.base.webservice.createServiceInteractions
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.contest.presenter.ButtonContestListener
import io.funx.futbol.core.webservice.builder
import io.funx.futbol.core.webservice.onError
import io.funx.futbol.core.webservice.onResponse
import io.funx.futbol.core.webservice.request

class ButtonContest : LinearLayoutCompat, ButtonContestListener {

    private val service by lazy {
        createServiceInteractions()
    }
    val imageButton: ImageButton
    private var userId: String = ""
    private var idProject: String = ""
    var normalFont: Typeface = Typeface.DEFAULT
    var boldFont: Typeface = Typeface.DEFAULT_BOLD
    var project: Project? = null
    var onResponse: (Project) -> Unit = {
        project = it
        statusButton(it)
        onClick(it, userId)
    }
    var onError: (Throwable) -> Unit = {}

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    init {
        orientation = VERTICAL
        imageButton = LayoutInflater.from(context).inflate(R.layout.image_button_contest, this, false) as ImageButton

        if (!ImageLoader.getInstance().isInited) {
            val displayImageOptions = DisplayImageOptions.Builder()
                    .build()

            val config = ImageLoaderConfiguration.Builder(context)
            config.threadPriority(Thread.NORM_PRIORITY - 2)
            config.denyCacheImageMultipleSizesInMemory()
            config.defaultDisplayImageOptions(displayImageOptions)
            config.diskCacheFileNameGenerator(Md5FileNameGenerator())
            config.diskCacheSize(50 * 1024 * 1024) // 50 MiB
            config.tasksProcessingOrder(QueueProcessingType.LIFO)

            ImageLoader.getInstance().init(config.build())
        }

        addView(imageButton)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == FunxContestDialog.RESULT_CONTEST) {
            imageButton.setOnClickListener(null)
            initButton(idProject, userId)
        }
    }

    @SuppressLint("CheckResult")
    fun initButton(idProject: String, userId: String) {
        this.idProject = idProject
        this.userId = userId
        service.getProjectAsync(idProject, userId).builder().onResponse {
            onResponse.invoke(it)
        }.onError {
            onError.invoke(it)
            it.printStackTrace()
        }.request()
    }

    private fun statusButton(project: Project) {
        ImageLoader.getInstance().displayImage(project.button, imageButton)
    }

    var dialog: FunxContestDialog = FunxContestDialog()

    private fun onClick(project: Project, userId: String) {
        imageButton.setOnClickListener {
            val frag = dialog
            frag.normalFont = normalFont
            frag.boldFont = boldFont
            frag.arguments = Bundle().apply {
                putString("uid", userId)
                putString("idProject", idProject)
                putParcelable("project", project)
            }
            (context as FragmentActivity?)?.let { act ->
                frag.show(act.supportFragmentManager, "TAG")
            }
        }
    }
}