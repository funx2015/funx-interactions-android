package io.funx.funxinteractions.contest.model

import com.google.gson.JsonObject
import io.funx.funxinteractions.base.webservice.createServiceInteractions
import kotlinx.coroutines.Deferred

class ContestModel {

    private var service = createServiceInteractions()

    fun onParticipateClick(idProject: String, user: String, event: String): Deferred<ResponseParticipate> {

        val data = JsonObject()
        data.addProperty("user", user)
        data.addProperty("event", event)

        return service.participateAsync(idProject, data)

    }
}
