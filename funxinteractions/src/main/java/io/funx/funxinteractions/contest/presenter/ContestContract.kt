package io.funx.funxinteractions.contest.presenter

import android.content.Context
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.contest.model.Finish
import io.funx.futbol.core.mvp.BaseMvpPresenter
import io.funx.futbol.core.mvp.BaseMvpView

interface ContestContract {

    interface View : BaseMvpView {
        fun showWelcome(project: Project)
        fun showTrivia(project: Project, uidUser: String)
        fun showFinish(project: Project)
        fun finishConcourse(project: Project?)
        fun showProgress()
        fun onResponseProject(project: Project)
        fun hideProgress()

    }

    interface Presenter : BaseMvpPresenter<View> {
        fun setProject(project: Project, uidUser: String, idProject: String)
        fun onParticipateClick()
        fun onBackPressed()
        fun onDismissPopUp(dismiss: Boolean, hasNext: Boolean)
        fun onResponseParticipate(finish: Finish?)
        fun getProject(context: Context, userId: String, idProject: String)

    }
}