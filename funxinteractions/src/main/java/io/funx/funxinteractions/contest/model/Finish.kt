package io.funx.funxinteractions.contest.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.funx.funxinteractions.content.model.FunxColor
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Finish(


        val title: String = "",

        val image: String = "",

        val text: String = "",

        @SerializedName("text_color")
        val textColor: FunxColor = FunxColor(),

        @SerializedName("content_color")
        val backgroundColor: FunxColor = FunxColor()

) : Parcelable