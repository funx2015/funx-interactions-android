package io.funx.funxinteractions.content.ui


import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import io.funx.funxinteractions.R

import io.funx.funxinteractions.content.model.Project

/**
 * Created by vitor on 8/28/17.
 */

class ViewEmpty(context: Context) : RelativeLayout(context) {

    private val layoutInflater: LayoutInflater = LayoutInflater.from(context)
    private lateinit var project: Project
    var normalFont: Typeface = Typeface.DEFAULT
    var boldFont: Typeface = Typeface.DEFAULT_BOLD


    fun setProject(project: Project) {
        this.project = project
        setLayout()
    }

    private fun setLayout() {

        layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT)

        val content = layoutInflater.inflate(R.layout.content_empty, this, false) as RelativeLayout
        addView(content)

        content.setBackgroundColor(project.colors.backgroundContent.color)

        val text = content.findViewById<TextView>(R.id.empty_text)
        text.setTextColor(project.colors.emptyTextColor.color)
        text.text = project.emptyText
        text.typeface = boldFont

    }


}
