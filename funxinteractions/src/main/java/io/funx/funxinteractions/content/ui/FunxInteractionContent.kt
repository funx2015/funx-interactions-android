package io.funx.funxinteractions.content.ui

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.View
import android.widget.ProgressBar
import android.widget.RelativeLayout
import androidx.appcompat.app.AppCompatActivity
import io.funx.funxinteractions.content.model.Colors
import io.funx.funxinteractions.content.model.FunxColor
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.content.presenter.ContentContract
import io.funx.funxinteractions.content.presenter.ContentPresenter
import io.funx.funxinteractions.content.presenter.ParticipateListener
import io.funx.funxinteractions.questions.model.VoteModel
import io.funx.funxinteractions.questions.ui.BaseQuestionsView


class FunxInteractionContent : RelativeLayout, ContentContract.View {

    private var mPresenter: ContentPresenter = ContentPresenter()
    private var progressBar: ProgressBar? = null
    var normalFont: Typeface = Typeface.DEFAULT
    var boldFont: Typeface = Typeface.DEFAULT_BOLD

    var idProject: String = ""
    var idRelation: String = ""
    var user: String = ""
    private var listenerParticipate: ParticipateListener? = null

    constructor(context: Context) : super(context) {
        mPresenter.attachView(this)
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mPresenter.attachView(this)
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) :
            super(context, attrs, defStyleAttr) {
        mPresenter.attachView(this)
    }

    override fun showLoader() {
        if (progressBar == null) {
            progressBar = mPresenter.getLoaderView()
            addView(progressBar)
        }
        progressBar?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressBar?.visibility = View.GONE
    }

    override fun context(): Context = context

    override fun activity(): AppCompatActivity = context as AppCompatActivity

    override fun error(project: Project?) {
        var project1: Project? = project
        if (project1 == null) {
            project1 = Project(
                    emptyText = "Intente de nuevo más tarde",
                    colors = Colors(
                            emptyTextColor = FunxColor("0-0-0-1"),
                            backgroundContent = FunxColor("255-255-255-1")
                    )
            )
        }
        val designEmpty = ViewEmpty(context)
        designEmpty.boldFont = boldFont
        designEmpty.normalFont = normalFont
        designEmpty.setProject(project1)

        addView(designEmpty)
    }

    override fun onErrorRequest(exception: Exception) {
        exception.printStackTrace()
    }

    fun setFonts(normal: Typeface, bold: Typeface) {
        this.normalFont = normal
        this.boldFont = bold
    }

    fun start(idProject: String, idRelation: String = "", user: String = "") {
        this.idProject = idProject
        this.idRelation = idRelation
        this.user = user
        mPresenter.initImageLoader()
        mPresenter.startQuestion(idProject, user, idRelation, normalFont, boldFont)

    }

    fun start(project: Project, user: String) {
        this.user = user
        mPresenter.initImageLoader()
        mPresenter.startQuestion(project, user, normalFont, boldFont)

    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mPresenter.detachView()
    }

    fun onVote(participateListener: (VoteModel?, Boolean, Boolean) -> Unit) {
        this.listenerParticipate = object : ParticipateListener {
            override fun onDismissPopUp(hasNext: Boolean) {
                participateListener.invoke(null, true, hasNext)
            }

            override fun onVote(vote: VoteModel) {
                participateListener.invoke(vote, false, false)
            }
        }
    }

    override fun showInteraction(interactionView: BaseQuestionsView) {
        removeAllViews()
        if (listenerParticipate != null) {
            interactionView.onResponseParticipate(listenerParticipate!!)
        }
        addView(interactionView)
    }
}
