package io.funx.funxinteractions.content.ui

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.FrameLayout
import android.widget.ViewFlipper
import androidx.appcompat.app.AppCompatActivity
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import io.funx.funxinteractions.R
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.contest.presenter.ContestContract
import io.funx.funxinteractions.contest.presenter.ContestPresenter
import kotlinx.android.synthetic.main.contest_finish.view.*
import kotlinx.android.synthetic.main.contest_welcome.view.*
import kotlinx.android.synthetic.main.funx_contest.view.*

@SuppressLint("InflateParams")
class FunxContestView : FrameLayout, ContestContract.View {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val imgLoader by lazy {
        if (!ImageLoader.getInstance().isInited) {
            val displayImageOptions = DisplayImageOptions.Builder()
                    .build()

            val config = ImageLoaderConfiguration.Builder(context)
            config.threadPriority(Thread.NORM_PRIORITY - 2)
            config.denyCacheImageMultipleSizesInMemory()
            config.defaultDisplayImageOptions(displayImageOptions)
            config.diskCacheFileNameGenerator(Md5FileNameGenerator())
            config.diskCacheSize(50 * 1024 * 1024) // 50 MiB
            config.tasksProcessingOrder(QueueProcessingType.LIFO)

            ImageLoader.getInstance().init(config.build())
        }
        ImageLoader.getInstance()
    }

    val layout by lazy { LayoutInflater.from(context).inflate(R.layout.funx_contest, null) }
    val contentViewFlipper by lazy { layout.findViewById<ViewFlipper>(R.id.contentViewFlipper) }
    val interactionView by lazy { layout.findViewById<FunxInteractionContent>(R.id.interactionView) }
    val progressContest by lazy { layout.findViewById<View>(R.id.progressContest) }
    var onResponse: (Project) -> Unit = {}
    var onFinishConcourse: (Project?) -> Unit = {}
    var mPresenter = ContestPresenter()
    var uidUser = ""
    var normalFont: Typeface = Typeface.DEFAULT
    var boldFont: Typeface = Typeface.DEFAULT_BOLD

    init {
        addView(layout)
        mPresenter.attachView(this)
        interactionView.onVote { vote, dismiss, hasNext ->
            if (vote == null) {
                mPresenter.onDismissPopUp(dismiss, hasNext)
            }
        }
    }

    @Deprecated("user cannot be empty")
    fun start(idProject: String, user: String = "") {
        uidUser = user
        mPresenter.getProject(context(), user, idProject)
    }

    fun start(project: Project, user: String) {
        uidUser = user
        mPresenter.setProject(project, uidUser = user, idProject = project.id.toString())
    }

    override fun showWelcome(project: Project) {
        contentViewFlipper?.visibility = View.VISIBLE
        if (project.skipWelcome) {
            showTrivia(project, uidUser)
            return
        }
        contentViewFlipper.displayedChild = 0
        val welcome = project.welcome
        imgLoader.displayImage(welcome.image, imageWelcome)
        titleWelcome.text = welcome.title
        subtitleWelcome.text = welcome.text
        titleToolbar.text = welcome.toolbarText
        titleToolbar.setTextColor(project.colors.optionTextColor.color)

        if (project.getState() == Project.States.INACTIVE) {
            btnParticipateWelcome.visibility = View.GONE
        } else {
            btnParticipateWelcome.visibility = View.VISIBLE
            btnParticipateWelcome.text = welcome.buttonText
            btnParticipateWelcome.setTextColor(project.colors.optionTextColor.color)
            btnParticipateWelcome.setBackgroundColor(project.colors.optionBackground.color)
            btnParticipateWelcome.setOnClickListener { mPresenter.onParticipateClick() }
        }
    }

    override fun showTrivia(project: Project, uidUser: String) {
        contentViewFlipper?.visibility = View.VISIBLE
        contentViewFlipper.displayedChild = 1
        interactionView.setFonts(normalFont, boldFont)
        interactionView.start(project, uidUser)
    }

    override fun showFinish(project: Project) {
        contentViewFlipper?.visibility = View.VISIBLE
        val finish = project.finish ?: return
        imgLoader.displayImage(finish.image, imageFinish)
        textFinish.text = finish.text
        titleFinish.text = finish.title
        titleFinish.setTextColor(finish.textColor.color)
        textFinish.setTextColor(finish.textColor.color)
        mainFinish.setBackgroundColor(finish.backgroundColor.color)
        contentViewFlipper.displayedChild = 2
    }

    override fun finishConcourse(project: Project?) {
        onFinishConcourse(project)
    }

    override fun showProgress() {
        progressContest?.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        progressContest?.visibility = View.GONE
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        mPresenter.detachView()
    }

    override fun onResponseProject(project: Project) {
        onResponse(project)
    }

    override fun onErrorRequest(exception: Exception) {
        exception.printStackTrace()
    }

    override fun context(): Context = this.context

    override fun activity(): AppCompatActivity = context as AppCompatActivity
}