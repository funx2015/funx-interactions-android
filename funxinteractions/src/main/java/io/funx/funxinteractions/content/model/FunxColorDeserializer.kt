package io.funx.funxinteractions.content.model

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type


class FunxColorDeserializer: JsonDeserializer<FunxColor> {

    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): FunxColor {

        val color = json?.asString ?: return FunxColor()

        return FunxColor(color)
    }

}