package io.funx.funxinteractions.content.model

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ContentService {

    @GET("/api/v2/get-project/{idProject}/")
    fun getProject(
            @Path("idProject") idProject: String,
            @Query("user") user: String,
            @Query("relation") relation: String = ""
    ): Deferred<Project>
}