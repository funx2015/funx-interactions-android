package io.funx.funxinteractions.content.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Colors(
        @SerializedName("top_text_background")
        var topTextBackground: FunxColor = FunxColor(),

        @SerializedName("top_text_color")
        var topTextColor: FunxColor = FunxColor(),

        @SerializedName("empty_text_color")
        var emptyTextColor: FunxColor = FunxColor(),

        @SerializedName("question_textcolor")
        var questionTextColor: FunxColor = FunxColor(),

        @SerializedName("option_background")
        var optionBackground: FunxColor = FunxColor(),

        @SerializedName("option_textcolor")
        var optionTextColor: FunxColor = FunxColor(),

        @SerializedName("option_background_error")
        var optionBackgroundError: FunxColor = FunxColor(),

        @SerializedName("option_textcolor_error")
        var optionTextColorError: FunxColor = FunxColor(),

        @SerializedName("option_background_correct")
        var optionBackgroundCorrect: FunxColor = FunxColor(),

        @SerializedName("option_textcolor_correct")
        var optionTextColorCorrect: FunxColor = FunxColor(),

        @SerializedName("background_all")
        var backgroundAll: FunxColor = FunxColor(),

        @SerializedName("background_content")
        var backgroundContent: FunxColor = FunxColor()

) : Parcelable