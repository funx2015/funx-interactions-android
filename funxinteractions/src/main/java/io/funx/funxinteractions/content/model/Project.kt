package io.funx.funxinteractions.content.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import io.funx.funxinteractions.contest.model.Finish
import io.funx.funxinteractions.contest.model.Welcome
import io.funx.funxinteractions.questions.model.Question
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Project(

        var name: String = "",

        var model: Int = 0,

        var id: Int = 0,

        var blockSwipe: Boolean = false,

        @SerializedName("top_text")
        var topText: String = "",

        @SerializedName("empty_text")
        var emptyText: String = "",

        var colors: Colors = Colors(),

        var skipWelcome: Boolean = false,

        var skipFinish: Boolean = false,

        var questions: ArrayList<Question> = arrayListOf(),

        @SerializedName("final")
        var finish: Finish? = null,

        var button: String = "",

        var state: String = "",

        var welcome: Welcome = Welcome()

) : Parcelable {

        enum class States {
                INACTIVE,
                ACTIVE,
                PARTIAL,
                PARTICIPATING,
                FINISHED
        }

        fun getState() : States = try {
                States.valueOf(state.toUpperCase())
        } catch (e: Exception) {
                e.printStackTrace()
                States.INACTIVE
        }


}