package io.funx.funxinteractions.content.presenter

import io.funx.funxinteractions.questions.model.VoteModel

interface ParticipateListener {

    fun onVote(vote: VoteModel)
    fun onDismissPopUp(hasNext: Boolean)
}