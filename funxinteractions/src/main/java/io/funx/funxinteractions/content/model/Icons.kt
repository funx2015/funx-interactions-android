package io.funx.funxinteractions.content.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class Icons(

        @SerializedName("button_active")
        var buttonActive: String = "",

        @SerializedName("button_clicked")
        var buttonClicked: String = "",

        @SerializedName("button_inactive")
        var buttonInactive: String = ""

) : Parcelable