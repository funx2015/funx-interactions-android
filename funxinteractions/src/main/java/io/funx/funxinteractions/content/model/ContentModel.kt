package io.funx.funxinteractions.content.model

import android.content.Context
import io.funx.funxinteractions.base.webservice.createServiceInteractions
import io.funx.funxinteractions.questions.ui.BaseQuestionsView
import io.funx.funxinteractions.questions.ui.ListQuestionsView
import io.funx.funxinteractions.questions.ui.PagerQuestionsView
import kotlinx.coroutines.Deferred

class ContentModel(val context: Context) {

    private val setupService = createServiceInteractions()

    fun getProject(idProject: String, user: String, relation: String = ""): Deferred<Project> {

        return setupService.getProjectAsync(idProject, user, relation)

    }

    fun getInteractionView(project: Project): BaseQuestionsView {
        return when (project.model) {
            1 -> PagerQuestionsView(context)
            2 -> ListQuestionsView(context)
            else -> PagerQuestionsView(context)
        }

    }


}