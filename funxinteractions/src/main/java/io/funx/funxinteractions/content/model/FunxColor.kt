package io.funx.funxinteractions.content.model

import android.os.Parcelable
import io.funx.funxinteractions.base.extensions.convertStringToColor
import kotlinx.android.parcel.IgnoredOnParcel
import kotlinx.android.parcel.Parcelize

@Parcelize
data class FunxColor(private var colorString: String = "") : Parcelable {

    @IgnoredOnParcel
    val color: Int
        get() {
            return convertStringToColor(colorString)
        }
}