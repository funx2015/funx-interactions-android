package io.funx.funxinteractions.content.presenter

import android.graphics.Typeface
import android.widget.ProgressBar
import io.funx.funxinteractions.content.model.Project
import io.funx.funxinteractions.questions.ui.BaseQuestionsView
import io.funx.futbol.core.mvp.BaseMvpPresenter
import io.funx.futbol.core.mvp.BaseMvpView

interface ContentContract {

    interface View : BaseMvpView {
        fun showLoader()
        fun hideProgress()
        fun showInteraction(interactionView: BaseQuestionsView)
        fun error(project: Project?)
    }

    interface Presenter : BaseMvpPresenter<View> {
        fun startQuestion(
                idProject: String,
                userId: String,
                relation: String,
                normalFont: Typeface?,
                boldFont: Typeface?

        )

        fun startQuestion(
                project: Project,
                userId: String,
                normalFont: Typeface?,
                boldFont: Typeface?

        )

        fun getLoaderView(): ProgressBar?
        fun initImageLoader()

    }

}