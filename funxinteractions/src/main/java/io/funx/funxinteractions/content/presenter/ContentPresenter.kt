package io.funx.funxinteractions.content.presenter

import android.annotation.SuppressLint
import android.graphics.Typeface
import android.widget.ProgressBar
import android.widget.RelativeLayout
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator
import com.nostra13.universalimageloader.core.DisplayImageOptions
import com.nostra13.universalimageloader.core.ImageLoader
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration
import com.nostra13.universalimageloader.core.assist.QueueProcessingType
import io.funx.funxinteractions.content.model.ContentModel
import io.funx.funxinteractions.content.model.Project
import io.funx.futbol.core.mvp.BaseMvpPresenterImpl
import io.funx.futbol.core.webservice.builder
import io.funx.futbol.core.webservice.onError
import io.funx.futbol.core.webservice.onResponse
import io.funx.futbol.core.webservice.request

class ContentPresenter : BaseMvpPresenterImpl<ContentContract.View>(), ContentContract.Presenter {

    private lateinit var model: ContentModel

    override fun getLoaderView(): ProgressBar? {
        val params = RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT)
        params.addRule(RelativeLayout.CENTER_IN_PARENT)

        val progressBar = ProgressBar(mView?.context(), null, android.R.attr.progressBarStyleLarge)
        progressBar.isIndeterminate = true

        return progressBar
    }

    override fun initImageLoader() {
        if (!ImageLoader.getInstance().isInited) {
            val displayImageOptions = DisplayImageOptions.Builder()
                    .build()

            val config = ImageLoaderConfiguration.Builder(mView?.context())
            config.threadPriority(Thread.NORM_PRIORITY - 2)
            config.denyCacheImageMultipleSizesInMemory()
            config.defaultDisplayImageOptions(displayImageOptions)
            config.diskCacheFileNameGenerator(Md5FileNameGenerator())
            config.diskCacheSize(50 * 1024 * 1024) // 50 MiB
            config.tasksProcessingOrder(QueueProcessingType.LIFO)

            ImageLoader.getInstance().init(config.build())
        }
    }

    @SuppressLint("CheckResult")
    override fun startQuestion(idProject: String,
                               userId: String,
                               relation: String,
                               normalFont: Typeface?,
                               boldFont: Typeface?
    ) {
        if (mView == null) return

        model = ContentModel(mView!!.context())
        model.getProject(idProject, userId, relation).builder()
                .onResponse {
                    val interactionView = model.getInteractionView(it)
                    interactionView.instanceProject(it, userId, normalFont, boldFont)
                    interactionView.initView()

                    if (it.questions.isEmpty()) {
                        mView?.error(it)
                    } else {
                        mView?.showInteraction(interactionView)
                    }
                }.onError {
                    mView?.error(null)
                }.request()

    }


    override fun startQuestion(project: Project, userId: String, normalFont: Typeface?, boldFont: Typeface?) {
        model = ContentModel(mView!!.context())
        val interactionView = model.getInteractionView(project)
        interactionView.instanceProject(project, userId, normalFont, boldFont)
        interactionView.initView()

        if (project.questions.isEmpty()) {
            mView?.error(project)
        } else {
            mView?.showInteraction(interactionView)
        }
    }


}
