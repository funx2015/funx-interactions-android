package io.funx.funxinteractionsexample

import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import io.funx.funxinteractions.base.Interaction
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var normal: Typeface
    private lateinit var bold: Typeface
    private var toolbar: Toolbar? = null
    private val interaction = Interaction("9", "user-demo-9")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar)

        toolbar?.title = ""
        setSupportActionBar(toolbar)

        normal = Typeface.createFromAsset(assets, "fonts/montserratRegular.ttf")
        bold = Typeface.createFromAsset(assets, "fonts/montserratBold.ttf")

        interaction.fontNormal = normal
        interaction.fontBold = bold

        interaction.init(btnParticipate)
        interaction.init(interaction1)
        interaction.containQuestion = {

        }



        interaction.onLoadFinish = { project ->
            if (project != null) {
                btnSample.setOnClickListener {
                    startActivity(Intent(this, SampleActivity::class.java).apply {
                        putExtra("project", project)
                    })
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.type1 -> {
                interaction.uidUserFirebase = "user-demo-10"
                interaction.init(interaction1)
            }

            R.id.type2 -> btnParticipate.initButton("7", "demo-6")
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        btnParticipate.onActivityResult(requestCode, resultCode, data)
    }
}
