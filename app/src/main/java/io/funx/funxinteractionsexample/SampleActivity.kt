package io.funx.funxinteractionsexample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import io.funx.funxinteractions.base.Interaction
import kotlinx.android.synthetic.main.activity_sample.*

class SampleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sample)

        val interaction = Interaction("14", "user-demo-2")
        interaction.load { project ->
            //project.blockSwipe = true
            project.let { interaction.init(funxContest, project) }
        }


    }
}
